<?php

/**
 * 核心模块语言包
 */
return array(
    '404' => '404 Page Not Found',
    'CLASS_LOST' => '类文件丢失',
    'ERROR_MES' => 'The requested URL was not found on this server.',
    'ERROR_FILE' => 'That’s all we know.',
);

