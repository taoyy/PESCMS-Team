<?php
/**
 * 在这里处理各种语言包文件
 * 使用这种形式调用文件
 * 便于用户利用程序进行本地化的操作。
 */
$pack = require 'pack.php';
$menu = require 'menu.php';
return array_merge($pack, $menu);

